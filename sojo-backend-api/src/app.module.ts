import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import dbConfig from './db.config';
import { AuthModule } from './auth/auth.module';
import { JobModule } from './job/job.module';
import { CategoryModule } from './category/category.module';
import { JobSeekerModule } from './job_seeker/job_seeker.module';
import { MulterModule } from '@nestjs/platform-express';
import { EmployerModule } from './employer/employer.module';
import { MailModule } from './mail/mail.module';
import { UserModule } from './user/user.module';
import { TestModule } from './test/test.module';
import { AdminAuthModule } from './admin/auth/admin_auth.module';
import { BlogModule } from './blog/blog.module';
import { AwsModule } from './aws/aws.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot(dbConfig),
        MulterModule.register({
            dest: './upload',
        }),
        AwsModule,
        EmployerModule,
        MailModule,
        AuthModule,
        JobSeekerModule,
        JobModule,
        CategoryModule,
        UserModule,
        TestModule,
        AdminAuthModule,
        BlogModule,
    ],
})
export class AppModule {}
