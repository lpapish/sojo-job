import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Category } from 'src/category/models/category.entity';
import { Job } from 'src/job/models/job.entity';
import { JobApplication } from 'src/job/models/job_applicantion.entity';
import { User } from 'src/user/models/user.entity';
import { Employer } from './employer/models/employer.entity';
import { JobInfo } from './job/models/job_info.entity';
import { JobTag } from './job/models/job_tags.entity';
import { JobSeeker } from './job_seeker/models/job_seeker.entity';
import { Resume } from './job_seeker/models/resume.entity';
import { JobExperience } from './job/models/job_experiences.entity';
import { JobEducation } from './job/models/job_education.entity';
import { Admin } from './admin/auth/models/admin.entity';
import { EmployerNew } from './employer/models/employer_new.entity';
import { Blog } from './blog/models/blog.entity';
import { JobQuestions } from './job/models/job_questions.entity';
import { JobApplicationQuestions } from './job/models/job_application_questions.entity';
import { JobUpdate } from './job/models/job_update.entity';

const dbConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'password',
    database: 'sojo',
    synchronize: true,
    entities: [
        Category,
        Job,
        JobUpdate,
        JobApplication,
        User,
        Employer,
        EmployerNew,
        JobInfo,
        JobTag,
        JobSeeker,
        Resume,
        JobExperience,
        JobEducation,
        JobQuestions,
        JobApplicationQuestions,
        Blog,

        Admin,
    ],
};

export default dbConfig;
