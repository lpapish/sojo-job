import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { join } from 'path';

@Module({
    imports: [
        MailerModule.forRoot({
            transport: {
                host: 'smtp.mailgun.org',
                port: 587,
                auth: {
                    user: 'postmaster@sandbox4b83d66671054593b33a6d6ec9512e04.mailgun.org',
                    pass: '92ac1b0e098e4cd8908915a5e56f2bc3-1b3a03f6-e5f32b21',
                },
            },
            defaults: {
                from: 'sojojob@gmail.com',
            },
            template: {
                dir: join(__dirname, 'templates'),
                adapter: new HandlebarsAdapter(),
                options: {
                    strict: true,
                },
            },
        }),
    ],
    providers: [MailService],
    exports: [MailService],
})
export class MailModule {}
