import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { Employer } from '../employer/models/employer.entity';

@Injectable()
export class MailService {
    constructor(private mailerService: MailerService) {}

    async sendUserConfirmation(employer: Employer, token: string) {
        try {
            const url = `example.com/auth/confirm?token=${token}`;

            await this.mailerService.sendMail({
                to: 'sojo.com',
                subject: 'Welcome to Nice App! Confirm your Email',
                template: 'confirmation',
                context: {
                    name: 'SojoJob',
                    url,
                },
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendForgotPasswordEmail(email: string, token: string) {
        try {
            const url = `http://localhost:3000/forgot-password?token=${token}`;

            await this.mailerService.sendMail({
                to: email,
                subject: 'Forgot Password',
                template: 'forgot_password',
                context: {
                    name: 'SojoJob',
                    url,
                },
            });

            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendEmployerProfileUpdateRejectEmail(email: string, reason: string) {
        try {
            await this.mailerService.sendMail({
                to: email,
                subject: 'Employer Profile Update Rejected',
                template: 'employer_profile_update_rejected',
                context: {
                    reason,
                },
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendEmployerProfileUpdateByAdminEmail(email: string) {
        try {
            await this.mailerService.sendMail({
                to: email,
                subject: 'Employer Profile Update',
                template: 'employer_profile_admin_updated',
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendEmployerProfileUpdateApprovedEmail(email: string) {
        try {
            await this.mailerService.sendMail({
                to: email,
                subject: 'Employer Profile Update Approved',
                template: 'employer_profile_update_approved',
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendAdminJobVerified(email: string) {
        try {
            await this.mailerService.sendMail({
                to: email,
                subject: 'Admin Verified Job',
                template: 'admin_job_verified',
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }

    async sendAdminNeedToAppoveJob() {
        try {
            await this.mailerService.sendMail({
                // change this email to admin email
                to: 'lpapish97@gmail.com',
                subject: 'Admin Approve Job',
                template: 'new_job_created',
            });
            console.log('sent');
        } catch (err) {
            console.log(err.message);
        }
    }
}
