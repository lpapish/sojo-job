import { Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';

@Injectable()
export class AwsService {
    private awsS3;
    constructor() {
        const s3 = new S3({
            accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
            secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
        });
        this.awsS3 = s3;
    }

    async uploadImage(file) {
        console.log(file);
        const uploadedImage = await this.awsS3
            .upload({
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: file.originalname,
                Body: file.buffer,
            })
            .promise();
        console.log(uploadedImage);
        return uploadedImage;
    }

    async getImageUrl(etag: string) {
        const url = await this.awsS3.getObject({
            Bucket: process.env.AWS_S3_BUCKET_NAME,
            Key: etag,
        });
        return url;
    }
}
