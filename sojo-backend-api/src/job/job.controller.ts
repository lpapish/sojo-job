import {
    Body,
    Controller,
    Get,
    Param,
    Post,
    UploadedFile,
    UseInterceptors,
    UseGuards,
    Request,
    Req,
    Response,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JobService } from './job.services';
import { JwtAuthGuard } from '../auth/jwt-strategy/jwt.guard';

@Controller('api/jobs')
export class JobController {
    constructor(private jobService: JobService) {}

    // ================== Public apis ===================
    /**
     * Fetch all jobs created so far
     *
     */
    @Get('all')
    getAllJobs(@Req() req) {
        return this.jobService.all(req.query);
    }

    @Get('all-jobs')
    getAllJobsList(@Req() req) {
        return this.jobService.allList(req.query);
    }

    @Get('info/:slug')
    getJobInfo(@Param('slug') slug) {
        return this.jobService.getSingleJobInfo(slug);
    }

    // ======================== Employer Apis =======================

    /**
     * Fetch jobs list for specific employer
     * For employer dashboard
     */
    @UseGuards(JwtAuthGuard)
    @Get('list')
    getJobsList(@Request() req) {
        return this.jobService.allJobs(req.user.sub);
    }

    /**
     * Fetch single job info
     */
    @UseGuards(JwtAuthGuard)
    @Get('/:slug/edit')
    getJobBySlug(@Param('slug') slug, @Request() req) {
        return this.jobService.getJobBySlug(slug, req.user.sub);
    }

    /**
     * Create Jobs for specific employer
     */
    @UseGuards(JwtAuthGuard)
    @Post('create')
    async createJob(@Request() req) {
        console.log(req.user.sub);
        return await this.jobService.createJob(req.body, req.user.sub);
    }

    @Post('create-admin')
    async createJobByAdmin(@Request() req) {
        return await this.jobService.createJobByAdmin(req.body);
    }

    /**
     * Update single job for specific employer
     */
    @UseGuards(JwtAuthGuard)
    @Post('/:slug/update')
    updateJob(@Request() req) {
        return this.jobService.updatedJob(req.body, req.params.slug);
    }

    // ====================== Job Seeker apis / Job Application =====================

    /**
     * Handle job applied by job_seeker
     *
     */
    @Post('apply-as-user')
    async applyAsUser(@Body() body, @Response() res) {
        const exist = await this.jobService.checkIfJobApplicationExist({
            job: body.job,
            user: body.user,
        });

        if (exist) {
            return res.send({
                status: 400,
                message: 'Already applied',
            });
        }

        this.jobService.createJobApplicationForUser({
            job: body.job,
            user: body.user,
            questions: body.questions,
        });
        return res.send({ status: 201 });
    }

    /**
     * Handle job applied by guest user
     *
     */
    @Post('apply')
    @UseInterceptors(FileInterceptor('file'))
    async uploadFile(@UploadedFile() file, @Body() body, @Response() res) {
        const data = {
            email: body.email,
            name: body.name,
            job: body.job,
            resume: file,
        };
        const exist = await this.jobService.validateGuestUserEmailExist(data);
        if (exist) {
            return res.send({
                status: 400,
                message: 'Already applied',
            });
        }
        const application = await this.jobService.createJobApplication(data);
        return res.send({
            status: 200,
            application,
            message: 'Applied',
        });
    }

    @Post('apply-guest-questions')
    submitGuestApplicationQuestions(@Request() req) {
        return this.jobService.submitGuestApplicationQuestions(req.body);
    }

    @Get('job-applicant-profile/:id')
    getJobApplicantProfile(@Request() req) {
        console.log('fetching application info');
        return this.jobService.getApplicantProfile(req.params.id);
    }

    @Post('/delete/:id')
    deleteJob(@Request() req) {
        return this.jobService.deleteJob(req.params.id);
    }

    @Get('/can-apply/:jobId/:userId')
    getJobApplicants(@Request() req) {
        return this.jobService.canUserApplyJob(
            req.params.jobId,
            req.params.userId,
        );
    }

    @Get('/detail/:jobId')
    getJobDetails(@Request() req) {
        return this.jobService.getJobDetails(req.params.jobId);
    }

    // ====================== Admin apis =====================
    @Post('/admin/job/:id/update')
    adminJobUpdate(@Request() req) {
        return this.jobService.adminJobUpdate(req.body, req.params.id);
    }
}
