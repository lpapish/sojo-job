import {
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';
import { JobApplication } from './job_applicantion.entity';
import { User } from '../../user/models/user.entity';
import { JobEducation } from './job_education.entity';
import { JobExperience } from './job_experiences.entity';
import { Category } from 'src/category/models/category.entity';
import { JobQuestions } from './job_questions.entity';

@Entity('jobs')
export class Job {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    description: string;

    @Column()
    slug: string;

    @Column()
    employment_type: string;

    @Column()
    location: string;

    @Column()
    salary: string;

    @Column()
    vacancy: string;

    @Column()
    tags: string;

    @Column()
    package: string;

    @Column({ default: false })
    is_verified: boolean;

    @CreateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP(6)',
    })
    created_at: Date;

    @UpdateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP(6)',
        onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    updated_at: Date;

    @ManyToOne(() => Category, (category) => category.jobs)
    category: Category;

    @OneToMany(() => JobApplication, (application) => application.job)
    applications: JobApplication[];

    @ManyToOne(() => User, (user) => user.jobs)
    user: User;

    @ManyToOne(() => JobEducation, (job_education) => job_education.job)
    education: JobEducation;

    @ManyToOne(() => JobExperience, (job_experience) => job_experience.job)
    experience: JobExperience;

    @OneToMany(() => JobQuestions, (job_questions) => job_questions.job)
    questions: JobQuestions[];
}
