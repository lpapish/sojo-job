import {
    Column,
    Entity,
    // JoinColumn,
    // OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('job_details')
export class JobDetail {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: 0,
    })
    views: number;

    @Column()
    salary: string;

    @Column()
    experience: string;

    // @OneToOne(() => Job, (job) => job.details, {
    //     onDelete: 'CASCADE',
    // })
    // @JoinColumn()
    // job: Job;
}
