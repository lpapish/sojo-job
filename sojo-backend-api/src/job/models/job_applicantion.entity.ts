import { User } from 'src/user/models/user.entity';
import {
    Column,
    Entity,
    ManyToOne,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Job } from './job.entity';
import { JobApplicationQuestions } from './job_application_questions.entity';

export enum APPLICATION_STATUS {
    FIT = 'shortlisted',
    NOT_FIT = 'not_shortlisted',
    HOLD = 'hold',
}

@Entity('job_applications')
export class JobApplication {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
    })
    email: string;

    @Column({
        nullable: true,
    })
    name: string;

    @Column({
        nullable: true,
    })
    resume: string;

    @Column({
        type: 'enum',
        enum: APPLICATION_STATUS,
        default: APPLICATION_STATUS.HOLD,
    })
    format: APPLICATION_STATUS;

    @Column({
        default: false,
    })
    viewed: boolean;

    @Column({
        type: 'longtext',
        nullable: true,
    })
    cover_letter: string;

    @ManyToOne(() => Job, (job) => job.applications, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    job: Job;

    @ManyToOne(() => User, (user) => user.job_applications, {
        onDelete: 'CASCADE',
        nullable: true,
    })
    user: User;

    @OneToMany(
        () => JobApplicationQuestions,
        (questions) => questions.job_application,
    )
    questions: JobApplicationQuestions[];
}
