import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('job_tags')
export class JobTag {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    key: string;

    @Column()
    value: string;
}
