import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Job } from './job.entity';
import { JobApplicationQuestions } from './job_application_questions.entity';

@Entity('job_questions')
export class JobQuestions {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Job, (job) => job.questions)
    job: Job;

    @Column()
    question: string;

    @Column()
    question_type: string;

    @Column()
    match_type: string;

    @OneToMany(
        () => JobApplicationQuestions,
        (application) => application.job_question,
        {
            onDelete: 'CASCADE',
        },
    )
    job_application_question: JobApplicationQuestions;
}
