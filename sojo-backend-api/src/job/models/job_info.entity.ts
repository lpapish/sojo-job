import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('job_infos')
export class JobInfo {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column({
        type: 'text',
    })
    name: string;
}
