import { JobSeeker } from 'src/job_seeker/models/job_seeker.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Job } from './job.entity';

@Entity('job_education')
export class JobEducation {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @OneToMany(() => JobSeeker, (job_seeker) => job_seeker.education)
    job_seeker: JobSeeker[];

    @OneToMany(() => Job, (job) => job.education)
    job: JobSeeker[];
}
