import {
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { JobApplication } from './job_applicantion.entity';
import { JobQuestions } from './job_questions.entity';

@Entity('job_application_questions')
export class JobApplicationQuestions {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    answer: string;

    @ManyToOne(
        () => JobApplication,
        (job_applicantion) => job_applicantion.questions,
        {
            onDelete: 'CASCADE',
            nullable: true,
        },
    )
    job_application: JobApplication;

    @ManyToOne(
        () => JobQuestions,
        (question) => question.job_application_question,
        {
            onDelete: 'CASCADE',
        },
    )
    job_question: JobQuestions;
}
