import { Controller, Get, Post, Request } from '@nestjs/common';
import { JobService } from './job.services';

@Controller('api/job-info')
export class JobInfoController {
    constructor(private jobService: JobService) {}

    @Get('/')
    index() {
        return this.jobService.getJobInfoList();
    }

    @Get('/tags')
    tags() {
        return this.jobService.getJobTags();
    }

    @Get('/experiences')
    experiences() {
        return this.jobService.getJobExperiences();
    }

    @Get('/educations')
    education() {
        return this.jobService.getJobEducation();
    }

    @Post('/education')
    createEducation(@Request() req) {
        return this.jobService.createEducation(req.body);
    }

    @Get('/education/:id')
    getEducation(@Request() req) {
        return this.jobService.getEducationById(req.params.id);
    }

    @Post('/education/:id')
    updateEducation(@Request() req) {
        return this.jobService.updateEducationById(req.params.id, req.body);
    }

    @Post('/education/:id')
    deleteEducation(@Request() req) {
        return this.jobService.deleteEducationById(req.params.id);
    }

    // Experience apis
    @Get('/experience')
    experience() {
        return this.jobService.getJobExperiences();
    }

    @Post('/experience')
    createExperience(@Request() req) {
        return this.jobService.createExperience(req.body);
    }

    @Get('/experience/:id')
    getExperience(@Request() req) {
        return this.jobService.getExperienceById(req.params.id);
    }

    @Post('/experience/:id')
    updateExperience(@Request() req) {
        return this.jobService.updateExperienceById(req.params.id, req.body);
    }

    // Presets
    @Post('/preset')
    createPreset(@Request() req) {
        return this.jobService.createPreset(req.body);
    }

    @Get('/preset/:id')
    getPreset(@Request() req) {
        return this.jobService.getPresetById(req.params.id);
    }

    @Post('/preset/:id')
    updatePreset(@Request() req) {
        return this.jobService.updatePresetById(req.params.id, req.body);
    }

    @Post('/questions/:jobId')
    addJobQuestions(@Request() req) {
        console.log(req.params.jobId, req.body);
        return this.jobService.addJobQuestions(req.params.jobId, req.body);
    }

    @Post('/questions/:jobId/update')
    updateJobQuestions(@Request() req) {
        console.log(req.params.jobId, req.body);
        return this.jobService.updateJobQuestions(req.params.jobId, req.body);
    }

    @Get('/questions/:jobId')
    getJobQuestions(@Request() req) {
        return this.jobService.getJobQuestions(req.params.jobId);
    }
}
