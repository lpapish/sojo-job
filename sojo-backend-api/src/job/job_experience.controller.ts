import { Controller, Get } from '@nestjs/common';
import { JobService } from './job.services';

@Controller('api/job-experiences')
export class JobInfoController {
    constructor(private jobService: JobService) {}

    @Get('/')
    index() {
        return this.jobService.getJobInfoList();
    }

    @Get('/tags')
    tags() {
        return this.jobService.getJobTags();
    }
}
