import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobController } from './job.controller';
import { JobInfoController } from './job_info.controller';
import { JobService } from './job.services';
import { Job } from './models/job.entity';
import { JobApplication } from './models/job_applicantion.entity';
import { JobInfo } from '../job/models/job_info.entity';
import { JobTag } from '../job/models/job_tags.entity';
import { JobExperience } from '../job/models/job_experiences.entity';
import { Employer } from '../employer/models/employer.entity';
import { User } from '../user/models/user.entity';
import { JobEducation } from './models/job_education.entity';
import { JobQuestions } from './models/job_questions.entity';
import { JobApplicationQuestions } from './models/job_application_questions.entity';
import { Resume } from 'src/job_seeker/models/resume.entity';
import { MailService } from 'src/mail/mail.service';
import { AwsService } from 'src/aws/aws.service';
import { JobUpdate } from './models/job_update.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Job,
            JobUpdate,
            JobApplication,
            JobInfo,
            JobTag,
            JobExperience,
            JobEducation,
            JobQuestions,
            JobApplicationQuestions,
            Employer,
            User,
            Resume,
        ]),
    ],
    providers: [JobService, MailService, AwsService],
    controllers: [JobController, JobInfoController],
})
export class JobModule {}
