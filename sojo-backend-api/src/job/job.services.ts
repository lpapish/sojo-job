import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Job } from './models/job.entity';
import { Employer } from '../employer/models/employer.entity';
import { JobApplication } from './models/job_applicantion.entity';
import { JobInfo } from './models/job_info.entity';
import { JobTag } from './models/job_tags.entity';
import { JobExperience } from './models/job_experiences.entity';
import { User } from '../user/models/user.entity';
import { JobEducation } from './models/job_education.entity';
import { JobQuestions } from './models/job_questions.entity';
import { JobApplicationQuestions } from './models/job_application_questions.entity';
import { Resume } from 'src/job_seeker/models/resume.entity';
import { MailService } from 'src/mail/mail.service';
import { AwsService } from 'src/aws/aws.service';
import { JobUpdate } from './models/job_update.entity';

@Injectable()
export class JobService {
    constructor(
        @InjectRepository(Employer)
        private readonly employerRepository: Repository<Employer>,
        @InjectRepository(Job) private readonly jobRepository: Repository<Job>,
        @InjectRepository(JobUpdate)
        private readonly jobUpdateRepository: Repository<JobUpdate>,
        @InjectRepository(JobApplication)
        private readonly jobApplicationRepository: Repository<JobApplication>,
        @InjectRepository(JobInfo)
        private readonly jobInfoRepository: Repository<JobInfo>,
        @InjectRepository(JobTag)
        private readonly jobTagRepository: Repository<JobTag>,
        @InjectRepository(JobExperience)
        private readonly jobExperienceRepository: Repository<JobExperience>,
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(JobEducation)
        private readonly jobEducationRepository: Repository<JobEducation>,
        @InjectRepository(JobQuestions)
        private readonly jobQuestionRepo: Repository<JobQuestions>,
        @InjectRepository(JobApplicationQuestions)
        private readonly jobApplicationQuestionRepo: Repository<JobApplicationQuestions>,
        @InjectRepository(Resume)
        private readonly resumeRepository: Repository<Resume>,
        private readonly mailService: MailService,
        private awsService: AwsService,
    ) {}

    async all(data) {
        const builder = await this.jobRepository.createQueryBuilder('job');
        await builder.leftJoinAndSelect('job.education', 'educations');
        await builder.leftJoinAndSelect('job.experience', 'experiences');
        await builder.leftJoinAndSelect('job.user', 'users');
        await builder.leftJoinAndSelect('users.employer', 'employers');

        if (data.keyword) {
            builder.where('job.title LIKE :s', {
                s: `%${data.keyword}%`,
            });
            builder.orWhere('employers.company_name LIKE :s', {
                s: `%${data.keyword}%`,
            });
        }

        if (data.category) {
            builder.where('job.category = :s', { s: data.category });
        }

        if (data.education) {
            builder.where('job.educationId = :s', { s: data.education });
        }

        if (data.experience) {
            builder.where('job.experienceId = :s', { s: data.experience });
        }

        return await builder
            .where('job.is_verified = :s', { s: true })
            .getMany();
    }

    async allList(data) {
        const builder = await this.jobRepository.createQueryBuilder('job');
        await builder.leftJoinAndSelect('job.education', 'educations');
        await builder.leftJoinAndSelect('job.experience', 'experiences');
        await builder.leftJoinAndSelect('job.user', 'users');
        await builder.leftJoinAndSelect('users.employer', 'employers');

        if (data.keyword) {
            builder.where('job.title LIKE :s', {
                s: `%${data.keyword}%`,
            });
            builder.orWhere('employers.company_name LIKE :s', {
                s: `%${data.keyword}%`,
            });
        }

        if (data.category) {
            builder.where('job.category = :s', { s: data.category });
        }

        if (data.education) {
            builder.where('job.educationId = :s', { s: data.education });
        }

        if (data.experience) {
            builder.where('job.experienceId = :s', { s: data.experience });
        }

        return await builder.getMany();
    }

    async getSingleJobInfo(slug) {
        const job = await this.jobRepository.findOne({
            where: {
                slug,
            },
            relations: [
                'applications',
                'user',
                'user.employer',
                'experience',
                'education',
                'questions',
            ],
        });

        return job;
    }

    jobsList() {
        return this.jobRepository.find();
    }

    async allJobs(userId) {
        const user = await this.userRepository.findOne({
            id: userId,
        });
        return this.jobRepository.find({
            where: {
                user,
            },
        });
    }

    async createJob(data, userId) {
        const user = await this.userRepository.findOne({
            where: { id: userId },
        });
        const dataMap = {
            title: data.title,
            description: data.description,
            slug:
                data.title.replace(' ', '-') +
                '-' +
                Math.floor(Math.random() * 100 + 1),
            user,
            employment_type: data.employment_type,
            location: data.location,
            experience: data.experience,
            salary: data.salary,
            vacancy: data.vacancy,
            tags: data.tags,
            category: data.category,
            package: data.package,
        };
        const job = this.jobRepository.create(dataMap);
        const newJOb = await this.jobRepository.save(job);

        const jobQuestion = await this.jobQuestionRepo.create({
            job: newJOb,
            question:
                'How many years of work experience do you have with [Skill] ?',
            match_type: '1',
            question_type: 'numeric',
        });
        await this.jobQuestionRepo.save(jobQuestion);
        this.mailService.sendAdminNeedToAppoveJob();
        return newJOb;
    }

    async createJobByAdmin(data) {
        const user = await this.userRepository.findOne({
            where: { id: data.userId },
        });
        const dataMap = {
            title: data.title,
            description: data.description,
            slug:
                data.title.replace(' ', '-') +
                '-' +
                Math.floor(Math.random() * 100 + 1),
            user,
            employment_type: data.employment_type,
            location: data.location,
            experience: data.experience,
            salary: data.salary,
            vacancy: data.vacancy,
            tags: data.tags,
            category: data.category,
            package: data.package,
        };
        const job = this.jobRepository.create(dataMap);
        const newJOb = await this.jobRepository.save(job);

        const jobQuestion = await this.jobQuestionRepo.create({
            job: newJOb,
            question:
                'How many years of work experience do you have with [Skill] ?',
            match_type: '1',
            question_type: 'numeric',
        });
        await this.jobQuestionRepo.save(jobQuestion);
        this.mailService.sendAdminNeedToAppoveJob();
        return newJOb;
    }

    async getJobBySlug(slug, userId) {
        const user = await this.userRepository.findOne({
            where: {
                id: userId,
            },
        });
        const job = await this.jobRepository.findOne({
            where: {
                slug,
                user,
            },
            relations: ['category', 'experience', 'education'],
        });
        return job;
    }

    async updatedJob(data, slug) {
        const job = await this.jobRepository.findOne({
            where: {
                slug,
            },
        });
        job.title = data.title;
        job.description = data.description;
        job.employment_type = data.employment_type;
        job.location = data.location;
        job.experience = data.experience;
        job.salary = data.salary;
        job.vacancy = data.vacancy;
        job.tags = data.tags;
        job.category = data.category;
        job.package = data.package;
        job.is_verified = false;

        this.mailService.sendAdminNeedToAppoveJob();

        return this.jobUpdateRepository.save(job);
    }

    async adminJobUpdate(data, id) {
        let job;

        const updateJob = await this.jobUpdateRepository.findOne(id);
        if (updateJob) {
            job = updateJob;
        } else {
            job = await this.jobRepository.findOne(id);
        }

        job.title = data.title;
        job.description = data.description;
        job.employment_type = data.employment_type;
        job.location = data.location;
        job.experience = data.experience;
        job.salary = data.salary;
        job.vacancy = data.vacancy;
        job.tags = data.tags;
        job.category = data.category;
        job.package = data.package;
        job.is_verified = data.is_verified;

        await this.jobRepository.save(job);

        const user = await this.userRepository.findOne(job.user);
        if (updateJob) {
            await this.jobUpdateRepository.delete(id);
        }
        this.mailService.sendAdminJobVerified(user.email);
    }

    async validateGuestUserEmailExist(data) {
        const exist = await this.jobApplicationRepository.findOne({
            where: {
                email: data.email,
            },
        });
        if (exist) return true;
        return false;
    }

    async createJobApplication(data) {
        if (data.resume) {
            const file = await this.awsService.uploadImage(data.resume);
            data.resume = file.Location;
        }
        const jobApplication = await this.jobApplicationRepository.create(data);
        return this.jobApplicationRepository.save(jobApplication);
    }

    async submitGuestApplicationQuestions(data) {
        const application = await this.jobApplicationRepository.findOne(
            data.application,
        );
        data.questions.forEach(async (quest) => {
            const queestionD = await this.jobQuestionRepo.findOne(
                quest.questionId,
            );
            const applicationQuestion =
                await this.jobApplicationQuestionRepo.create({
                    answer: quest.answer,
                    job_application: application,
                    job_question: queestionD,
                });
            await this.jobApplicationQuestionRepo.save(applicationQuestion);
        });
    }

    async checkIfJobApplicationExist(data) {
        const exist = await this.jobApplicationRepository.findOne({
            where: {
                job: data.job,
                user: data.user,
            },
        });

        if (exist) {
            return true;
        }

        return false;
    }

    async createJobApplicationForUser(data) {
        const body = {
            job: data.job,
            user: data.user,
        };
        const jobApplication = await this.jobApplicationRepository.create(body);
        await this.jobApplicationRepository.save(jobApplication);

        data.questions.forEach(async (question) => {
            const questionD = await this.jobQuestionRepo.findOne(
                question.questionId,
            );
            const applicationQuestion =
                await this.jobApplicationQuestionRepo.create({
                    answer: question.answer,
                    job_application: jobApplication,
                    job_question: questionD,
                });
            await this.jobApplicationQuestionRepo.save(applicationQuestion);
        });
    }

    getJobInfoList() {
        return this.jobInfoRepository.find();
    }

    getJobTags() {
        return this.jobTagRepository.find();
    }

    getJobExperiences() {
        return this.jobExperienceRepository.find({});
    }

    getJobEducation() {
        return this.jobEducationRepository.find({});
    }

    async getApplicantProfile(id) {
        const data = await this.jobApplicationRepository.findOne(id, {
            relations: ['user', 'user.jobSeeker'],
        });
        return data;
    }

    async deleteJob(id) {
        const job = await this.jobRepository.findOne(id);
        return this.jobRepository.remove(job);
    }

    async canUserApplyJob(jobId, userId) {
        const user = await this.resumeRepository.findOne({
            where: {
                user: userId,
            },
        });
        const job = await this.jobApplicationRepository.find({
            where: {
                job: jobId,
                user: userId,
            },
        });
        if (job && job.length > 0) {
            return true;
        }
        if (user?.experience === '') {
            return true;
        }
        return false;
    }

    async getJobDetails(jobId) {
        // find if user have updated info, if update job is pending return that info
        const updateJob = await this.jobUpdateRepository.findOne({
            where: {
                id: jobId,
            },
            relations: ['education', 'experience', 'category'],
        });

        if (updateJob) {
            return updateJob;
        }

        const job = await this.jobRepository.findOne({
            where: {
                id: jobId,
            },
            relations: ['education', 'experience', 'category'],
        });

        return job;
    }

    async createEducation(data) {
        const education = await this.jobEducationRepository.create(data);
        return this.jobEducationRepository.save(education);
    }

    async getEducationById(id) {
        return this.jobEducationRepository.findOne(id);
    }

    async updateEducationById(id, data) {
        const education = await this.jobEducationRepository.findOne(id);
        education.name = data.name;
        return this.jobEducationRepository.save(education);
    }

    async deleteEducationById(id) {
        const education = await this.jobEducationRepository.findOne(id);
        return this.jobEducationRepository.remove(education);
    }

    async createExperience(data) {
        const experience = await this.jobExperienceRepository.create(data);
        return this.jobExperienceRepository.save(experience);
    }

    async getExperienceById(id) {
        return this.jobExperienceRepository.findOne(id);
    }

    async updateExperienceById(id, data) {
        const experience = await this.jobExperienceRepository.findOne(id);
        experience.name = data.name;
        return this.jobExperienceRepository.save(experience);
    }

    async createPreset(data) {
        const preset = await this.jobInfoRepository.create(data);
        return this.jobInfoRepository.save(preset);
    }

    async getPresetById(id) {
        return this.jobInfoRepository.findOne(id);
    }

    async updatePresetById(id, data) {
        const preset = await this.jobInfoRepository.findOne(id);
        preset.title = data.title;
        preset.name = data.name;
        return this.jobInfoRepository.save(preset);
    }

    async addJobQuestions(jobId, data) {
        const job = await this.jobRepository.findOne(jobId);
        const question = await this.jobQuestionRepo.create({
            question: data.question,
            question_type: data.question_type,
            match_type: data.match_type,
            job,
        });
        return this.jobQuestionRepo.save(question);
    }

    async updateJobQuestions(jobId, data) {
        const update = await this.jobQuestionRepo.findOne(data.id);

        update.question = data.question;
        update.match_type = data.match_type;

        return this.jobQuestionRepo.save(update);
    }

    async getJobQuestions(jobId) {
        const job = await this.jobRepository.findOne(jobId);
        return this.jobQuestionRepo.find({
            where: {
                job,
            },
        });
    }
}
