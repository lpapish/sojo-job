import {
    Body,
    Controller,
    Get,
    Param,
    Post,
    Request,
    Res,
    UploadedFile,
    UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import path = require('path');
import { BlogService } from './blog.service';

@Controller('api/blog')
export class BlogController {
    constructor(private readonly blogService: BlogService) {}

    @Get('/all')
    async index() {
        return this.blogService.getBlogs();
    }

    @Post('/create')
    @UseInterceptors(
        FileInterceptor('blog', {
            storage: diskStorage({
                destination: './upload/blog',
                filename: (req, file, cb) => {
                    const filename: string =
                        path.parse(file.originalname).name.replace(/\s/g, '') +
                        Math.floor(Math.random() * 1000000);
                    const extension: string = path.parse(file.originalname).ext;
                    cb(null, `${filename}${extension}`);
                },
            }),
        }),
    )
    async create(@UploadedFile() file: Express.Multer.File, @Body() body) {
        body.banner = file.filename;
        return this.blogService.createBlog(body);
    }

    @Get('/edit/:id')
    async edit(@Request() req) {
        return this.blogService.editBlog(req.params.id);
    }

    @Post('/update/:id')
    @UseInterceptors(
        FileInterceptor('blog', {
            storage: diskStorage({
                destination: './upload/blog',
                filename: (req, file, cb) => {
                    const filename: string =
                        path.parse(file.originalname).name.replace(/\s/g, '') +
                        Math.floor(Math.random() * 1000000);
                    const extension: string = path.parse(file.originalname).ext;
                    cb(null, `${filename}${extension}`);
                },
            }),
        }),
    )
    async update(
        @UploadedFile() file: Express.Multer.File,
        @Body() body,
        @Request() req,
    ) {
        if (file && file.filename) {
            body.banner = file.filename;
        }
        return this.blogService.updateBlog(req.params.id, body);
    }

    @Get('banner/:image')
    blogBanner(@Param('image') image, @Res() res) {
        return res.sendFile(image, { root: './upload/blog' });
    }
}
