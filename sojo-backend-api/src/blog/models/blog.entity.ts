import { Admin } from 'src/admin/auth/models/admin.entity';
import {
    Column,
    CreateDateColumn,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity('blogs')
export class Blog {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: '',
    })
    banner: string;

    @Column()
    permalink: string;

    @Column()
    title: string;

    @Column({
        type: 'longtext',
    })
    content: string;

    @OneToOne(() => Admin, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    created_by: Admin;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}
