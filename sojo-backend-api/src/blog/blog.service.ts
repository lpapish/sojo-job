import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Blog } from './models/blog.entity';

@Injectable()
export class BlogService {
    constructor(
        @InjectRepository(Blog)
        private readonly blogRepository: Repository<Blog>,
    ) {}

    getBlogs() {
        return this.blogRepository.find({});
    }

    createBlog(blog: Blog) {
        return this.blogRepository.save(blog);
    }

    editBlog(id) {
        return this.blogRepository.findOne({
            where: {
                permalink: id,
            },
            relations: ['created_by'],
        });
    }

    async updateBlog(id, blog: Blog) {
        const ublog = await this.blogRepository.findOne(id);
        ublog.title = blog.title;
        ublog.content = blog.content;
        ublog.banner = blog.banner;
        ublog.permalink = blog.permalink;
        return this.blogRepository.save(ublog);
    }
}
