import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';
import { Blog } from './models/blog.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Blog])],
    providers: [BlogService],
    controllers: [BlogController],
})
export class BlogModule {}
