import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/category/models/category.entity';
import { JobEducation } from 'src/job/models/job_education.entity';
import { JobExperience } from 'src/job/models/job_experiences.entity';
import { JobInfo } from 'src/job/models/job_info.entity';
import { JobTag } from 'src/job/models/job_tags.entity';
import { Repository } from 'typeorm';
import { User } from './models/user.entity';

type decodedUser = {
    userId: number;
};
@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,

        @InjectRepository(JobEducation)
        private readonly jobEducationRepository: Repository<JobEducation>,

        @InjectRepository(JobExperience)
        private readonly jobExperienceRepository: Repository<JobExperience>,

        @InjectRepository(Category)
        private readonly categoryRepository: Repository<Category>,

        @InjectRepository(JobTag)
        private readonly jobTagRepository: Repository<JobTag>,

        @InjectRepository(JobInfo)
        private readonly jobInfoRepository: Repository<JobInfo>,

        private jwtService: JwtService,
    ) {}

    getUserProfile(userId) {
        return this.userRepository.findOne({
            where: {
                id: userId,
            },
            relations: ['jobSeeker', 'jobSeeker.category'],
        });
    }

    async createMockData() {
        const eduArr = [
            'Education - 10 (Above & Below)',
            '+2 Degree',
            'Bachelor Degree',
            'Master',
        ];
        eduArr.forEach(async (education) => {
            const edu1 = await this.jobEducationRepository.create({
                name: education,
            });
            await this.jobEducationRepository.save(edu1);
        });

        const expArr = ['Junior', 'Mid Level', 'Senior', 'Intern', 'Contract'];
        expArr.forEach(async (exp) => {
            const expReq = await this.jobExperienceRepository.create({
                name: exp,
            });
            await this.jobExperienceRepository.save(expReq);
        });

        const categoryArr = [
            {
                name: 'IT & Communication',
                parentId: null,
            },
            {
                name: 'Software Development',
                parentId: null,
            },
            {
                name: 'Banks',
                parentId: null,
            },
            {
                name: 'Insurance & Finance',
                parentId: null,
            },
        ];
        categoryArr.forEach(async (category) => {
            const cat = await this.categoryRepository.create({
                name: category.name,
            });
            await this.categoryRepository.save(cat);
        });

        const tagArr = [
            {
                key: 'web-development',
                value: 'Web Development',
            },
            {
                key: 'c++',
                value: 'C++',
            },
        ];
        tagArr.forEach(async (tage) => {
            const tag = await this.jobTagRepository.create({
                key: tage.key,
                value: tage.value,
            });
            await this.jobTagRepository.save(tag);
        });

        const jobInfos = [
            {
                title: 'Software Engineer',
                name: 'Test',
            },
        ];
        jobInfos.forEach(async (ee) => {
            const info = await this.jobInfoRepository.create({
                title: ee.title,
                name: ee.name,
            });
            await this.jobInfoRepository.save(info);
        });
    }

    async forgotPasswordToken(data) {
        const user = await this.userRepository.findOne({
            where: {
                email: data.email,
            },
        });

        const payload = {
            email: data.email,
            userId: user.id,
        };

        return await this.jwtService.sign(payload);
    }

    /* eslint-disable  */
    async resetPassword(data, token) {
        const t = token.split(' ');
        const d: any = this.jwtService.decode(t[1]);
        const user = await this.userRepository.findOne(d.userId);
        user.password = data.password;
        return await this.userRepository.save(user);
    }
}
