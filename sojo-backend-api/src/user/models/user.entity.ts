import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToMany,
    OneToOne,
} from 'typeorm';
import { Job } from '../../job/models/job.entity';
import { JobApplication } from 'src/job/models/job_applicantion.entity';
import { JobSeeker } from 'src/job_seeker/models/job_seeker.entity';
import { Employer } from '../../employer/models/employer.entity';
import { EmployerNew } from 'src/employer/models/employer_new.entity';

export enum UserTypes {
    ADMIN = 'admin',
    EMPLOYER = 'employer',
    JOB_SEEKER = 'job_seeker',
}

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    username: string;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({
        type: 'enum',
        enum: UserTypes,
    })
    user_type: UserTypes;

    @OneToOne(() => JobSeeker, (job_seeker) => job_seeker.user)
    jobSeeker: JobSeeker;

    @OneToOne(() => Employer, (employer) => employer.user)
    employer: Employer;

    @OneToOne(() => EmployerNew, (employerNew) => employerNew.user)
    employerNew: Employer;

    @OneToMany(() => JobApplication, (application) => application.user)
    job_applications: JobApplication[];

    @OneToMany(() => Job, (job) => job.user)
    jobs: Job[];
}
