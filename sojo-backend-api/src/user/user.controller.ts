import { Controller, Get, Post, Request } from '@nestjs/common';
import { MailService } from 'src/mail/mail.service';
import { UserService } from './user.service';

@Controller('api/user')
export class UserController {
    constructor(
        private userService: UserService,
        private mailService: MailService,
    ) {}

    @Get('/profile')
    getUserProfile(@Request() req) {
        return this.userService.getUserProfile(req.query.userId);
    }

    // mock api for testing, delete later
    @Get('/mock')
    createMockData() {
        return this.userService.createMockData();
    }

    @Post('forgot-password')
    async forgotPassword(@Request() req) {
        const token = await this.userService.forgotPasswordToken(req.body);
        await this.mailService.sendForgotPasswordEmail(req.body.email, token);
        return 'ok';
    }

    @Post('reset_password')
    async resetPassword(@Request() req) {
        await this.userService.resetPassword(
            req.body,
            req.headers.authorization,
        );
        return 'ok';
    }
}
