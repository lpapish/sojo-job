import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Category } from 'src/category/models/category.entity';
import { JobEducation } from 'src/job/models/job_education.entity';
import { JobExperience } from 'src/job/models/job_experiences.entity';
import { JobInfo } from 'src/job/models/job_info.entity';
import { JobTag } from 'src/job/models/job_tags.entity';
import { MailService } from 'src/mail/mail.service';
import { User } from './models/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User,
            JobEducation,
            JobExperience,
            Category,
            JobTag,
            JobInfo,
        ]),
        JwtModule.register({
            secret: 'some-random-secret',
            signOptions: { expiresIn: '60m' },
        }),
    ],
    controllers: [UserController],
    providers: [UserService, MailService],
})
export class UserModule {}
