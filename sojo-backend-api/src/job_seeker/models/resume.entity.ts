import { User } from '../../user/models/user.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
} from 'typeorm';

@Entity('resumes')
export class Resume {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: '',
    })
    fullname: string;

    @Column({
        default: '',
    })
    current_address: string;

    @Column({
        default: '',
    })
    contact_no: string;

    @Column({
        default: '',
    })
    summary: string;

    @Column({
        default: '',
    })
    experience: string;

    @Column({
        default: '',
    })
    education: string;

    @Column({
        default: '',
    })
    skills: string;

    @Column({
        default: '',
    })
    activities: string;

    @OneToOne(() => User, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    user: User;
}
