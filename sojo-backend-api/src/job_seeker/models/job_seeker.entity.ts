import { User } from '../../user/models/user.entity';
import { Category } from '../../category/models/category.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
    ManyToOne,
} from 'typeorm';
import { JobExperience } from 'src/job/models/job_experiences.entity';
import { JobEducation } from 'src/job/models/job_education.entity';

@Entity('job_seekers')
export class JobSeeker {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: '',
    })
    fullname: string;

    @Column({
        default: '',
    })
    current_address: string;

    @Column({
        default: '',
    })
    permanent_address: string;

    @Column({
        default: '',
    })
    contact_no: string;

    @Column({
        default: '',
    })
    dob: string;

    @Column({
        default: '',
    })
    profile_image: string;

    @Column({
        default: false,
    })
    relocate: boolean;

    @Column({
        default: '',
    })
    tags: string;

    @ManyToOne(() => JobEducation, (job_education) => job_education.job_seeker)
    education: JobEducation;

    @ManyToOne(() => Category, (category) => category.jobSeeker)
    category: Category;

    @ManyToOne(() => JobExperience, (experience) => experience.job_seeker)
    experience: JobExperience;

    @OneToOne(() => User, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    user: User;
}
