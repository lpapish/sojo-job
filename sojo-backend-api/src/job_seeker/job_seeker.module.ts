import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobSeekerController } from './job_seeker.controller';
import { JobSeekerService } from './job_seeker.service';
import { JobSeeker } from './models/job_seeker.entity';
import { Resume } from './models/resume.entity';
import { User } from '../user/models/user.entity';
import { AwsService } from 'src/aws/aws.service';

@Module({
    imports: [TypeOrmModule.forFeature([JobSeeker, Resume, User])],
    providers: [JobSeekerService, AwsService],
    controllers: [JobSeekerController],
})
export class JobSeekerModule {}
