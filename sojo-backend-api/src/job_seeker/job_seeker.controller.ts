import {
    Controller,
    Request,
    Post,
    Get,
    UseInterceptors,
    UploadedFile,
    Param,
    Res,
    Body,
    Response,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JobSeekerService } from './job_seeker.service';

@Controller('api/job_seeker')
export class JobSeekerController {
    constructor(private jobSeekerService: JobSeekerService) {}

    @Post('udpate-profile')
    async update_profile(@Request() req) {
        return this.jobSeekerService.updateProfile(req.body);
    }

    @Get(':id/profile')
    async getProfileInfo(@Request() req) {
        return this.jobSeekerService.getProfile(req.params.id);
    }

    @Post('update-resume')
    async update_resume(@Request() req) {
        return this.jobSeekerService.updateResume(req.body);
    }

    @Get(':id/resume')
    async getResume(@Request() req) {
        return this.jobSeekerService.getResume(req.params.id);
    }

    @Post('upload-profile-image')
    @UseInterceptors(FileInterceptor('job-seeker-profile'))
    async uploadProfileImage(
        @UploadedFile() file,
        @Body() body,
        @Response() res,
    ) {
        console.log(file);
        const ress = await this.jobSeekerService.updateProfileImage(
            body.user,
            file,
        );
        res.send({
            status: 200,
            message: 'Uploaded',
            data: {
                image: ress,
            },
        });
    }

    @Get('image/:imgpath')
    seeUploadedFile(@Param('imgpath') image, @Res() res) {
        return res.sendFile(image, { root: './upload/company/profile' });
    }

    @Get('applied-jobs/:id')
    appliedJobs(@Request() req) {
        return this.jobSeekerService.appliedJobs(req.params.id);
    }

    @Post('delete-profile-image/:id')
    deleteProfileImage(@Request() req) {
        return this.jobSeekerService.deleteProfileImage(req.params.id);
    }

    @Post('reset_password/:id')
    resetPassword(@Request() req) {
        return this.jobSeekerService.resetPassword(req.body, req.params.id);
    }

    @Get('resume/:filename')
    getUploadedResumeFile(@Param('filename') image, @Res() res) {
        return res.sendFile(image, { root: './upload/' });
    }
}
