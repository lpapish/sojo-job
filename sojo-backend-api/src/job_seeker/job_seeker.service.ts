import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JobSeeker } from './models/job_seeker.entity';
import { Resume } from './models/resume.entity';
import { User } from '../user/models/user.entity';
import { AwsService } from 'src/aws/aws.service';

@Injectable()
export class JobSeekerService {
    constructor(
        @InjectRepository(JobSeeker)
        private readonly jobSeekerRepository: Repository<JobSeeker>,
        @InjectRepository(Resume)
        private readonly resumeRepository: Repository<Resume>,
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private awsService: AwsService,
    ) {}

    async updateProfile(data) {
        const postData = {
            fullname: data.fullname,
            current_address: data.current_address,
            permanent_address: data.permanent_address,
            contact_no: data.contact_no,
            dob: data.dob,
            education: data.education,
            experience: data.experience,
            tags: data.tags,
            user: data.user,
            category: data.job_category,
            relocate: data.relocate,
            profile_image: data.profile_image,
        };

        const user = await this.userRepository.findOne({
            where: {
                id: data.user,
            },
        });

        const exist = await this.jobSeekerRepository.findOne({
            where: {
                user,
            },
        });

        if (exist) {
            exist.fullname = data.fullname;
            exist.current_address = data.current_address;
            exist.permanent_address = data.permanent_address;
            exist.contact_no = data.contact_no;
            exist.dob = data.dob;

            if (data.education !== '') {
                exist.education = data.education;
            }

            if (data.job_category !== '') {
                exist.category = data.job_category;
            }

            if (data.experience !== '') {
                exist.experience = data.experience;
            }
            exist.tags = data.tags;
            return this.jobSeekerRepository.save(exist);
        } else {
            const newProfile = await this.jobSeekerRepository.create(postData);
            return this.jobSeekerRepository.save(newProfile);
        }
    }

    async getProfile(user) {
        return this.jobSeekerRepository.findOne({
            where: {
                user,
            },
            relations: ['education', 'category', 'experience'],
        });
    }

    async updateResume(data) {
        const postData = {
            fullname: data.fullname,
            current_address: data.current_address,
            contact_no: data.contact_no,
            summary: data.summary,
            experience: data.experience,
            education: data.education,
            skills: data.skills,
            activities: data.activities,
            user: data.user,
        };

        const exist = await this.resumeRepository.findOne({
            where: {
                user: data.user,
            },
        });

        if (exist) {
            exist.fullname = data.fullname;
            exist.current_address = data.current_address;
            exist.contact_no = data.contact_no;
            exist.summary = data.summary;
            exist.experience = data.experience;
            exist.education = data.education;
            exist.skills = data.skills;
            exist.activities = data.activities;
            return this.resumeRepository.save(exist);
        } else {
            const d = await this.resumeRepository.create(postData);
            return this.resumeRepository.save(d);
        }
    }

    async getResume(user) {
        return this.resumeRepository.findOne({
            where: {
                user,
            },
        });
    }

    async appliedJobs(id) {
        const user = await this.userRepository.find({
            where: {
                id,
            },
            relations: ['job_applications', 'job_applications.job'],
        });
        return user;
    }

    async deleteProfileImage(id) {
        const user = await this.userRepository.findOne({
            where: {
                id,
            },
            relations: ['jobSeeker'],
        });

        const jobSeeker = await this.jobSeekerRepository.findOne({
            where: {
                user,
            },
        });

        jobSeeker.profile_image = '';
        return this.jobSeekerRepository.save(jobSeeker);
    }

    async updateProfileImage(user, profileImage) {
        const file = await this.awsService.uploadImage(profileImage);
        const jobSeeker = await this.jobSeekerRepository.findOne({
            where: {
                user,
            },
        });
        jobSeeker.profile_image = file.Location;
        await this.jobSeekerRepository.save(jobSeeker);
        return file.Location;
    }

    async resetPassword(data, userId) {
        const user = await this.userRepository.findOne(userId);
        user.password = data.password;
        return this.userRepository.save(user);
    }
}
