import {
    Body,
    Controller,
    Get,
    Param,
    Request,
    Post,
    Res,
    UploadedFile,
    UseInterceptors,
    UseGuards,
    Response,
    UploadedFiles,
} from '@nestjs/common';
import { AnyFilesInterceptor, FileInterceptor } from '@nestjs/platform-express';
// import { LocalAuthGuard } from '../auth/local-strategy/local.guard';
import { JwtAuthGuard } from '../auth/jwt-strategy/jwt.guard';
import { diskStorage } from 'multer';
import { EmployerService } from './employer.service';
import { uuid } from 'uuidv4';
import path = require('path');

@Controller('api/employer')
export class EmployerController {
    constructor(private employerService: EmployerService) {}

    @Get('/profile/:id')
    profile(@Param('id') id) {
        return this.employerService.getEmployerInfo(id);
    }

    @Get('/info/:id')
    profileInfo(@Param('id') id) {
        return this.employerService.getEmployerDetailInfo(id);
    }

    @Get('/profile/:id/image')
    async profileImage(@Param('id') id, @Res() res: any) {
        const employer = await this.employerService.getEmployerInfo(id);
        res.sendFile(employer.logo, {
            root: 'upload/company/profile',
        });
    }

    @Post('/profile/:id')
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './upload/company/profile',
                filename: (req, file, cb) => {
                    const filename: string =
                        path.parse(file.originalname).name.replace(/\s/g, '') +
                        uuid();
                    const extension: string = path.parse(file.originalname).ext;

                    cb(null, `${filename}${extension}`);
                },
            }),
        }),
    )
    async uploadFile(
        @UploadedFile() file: Express.Multer.File,
        @Body() body,
        @Param('id') id,
    ) {
        const profile = await this.employerService.updateProfile(id, {
            aboutUs: body.aboutUs,
            scope: body.scope,
            info_founded_date: body.info_founded_date,
            info_headquaters: body.info_headquaters,
            info_company_size: body.info_company_size,
            info_website_url: body.info_website_url,
        });

        if (file) {
            await this.employerService.updateProfileLogo(id, file);
        }

        return profile;
    }

    /* eslint-disable */
    @Post('/:id/update')
    @UseInterceptors(AnyFilesInterceptor())
    updateEmployerInfo(@UploadedFiles() files, @Body() body, @Param('id') id) {
        const logo = files.find((file) => file.fieldname === 'logo') || null;
        const banner =
            files.find((file) => file.fieldname === 'banner') || null;
        return this.employerService.updateEmployer(id, body, logo, banner);
    }

    @Post('/:id/update/admin')
    updateEmployerInfoAdmin(@Body() body, @Param('id') id) {
        return this.employerService.updateEmployerAdmin(id, body);
    }

    @UseGuards(JwtAuthGuard)
    @Get('jobs')
    getJobs(@Request() req) {
        return this.employerService.getJobs(req.user.sub);
    }

    @Get('company-info-jobs/:id')
    getCompanyInfoWithJobs(@Request() req) {
        return this.employerService.getCompanyInfoWithJobs(req.params.id);
    }

    /**
     * handle profile image upload for employer
     *
     */
    @Post('upload/profile-image')
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './upload/employer/profile',
            }),
        }),
    )
    async uploadProfileImage(
        @UploadedFile() file: Express.Multer.File,
        @Body() body,
        @Response() res,
    ) {
        await this.employerService.updateProfileImage(body.user, file.filename);
        res.send({
            status: 200,
            message: 'Uploaded',
        });
    }

    @Get('image/:imgpath')
    seeUploadedFile(@Param('imgpath') image, @Res() res) {
        return res.sendFile(image, { root: './upload/employer/profile' });
    }

    /**
     * handle profile image upload for employer
     *
     */
    @Post('upload/banner-image')
    @UseInterceptors(
        FileInterceptor('file', {
            storage: diskStorage({
                destination: './upload/employer/banner',
            }),
        }),
    )
    async uploadBannerImage(
        @UploadedFile() file: Express.Multer.File,
        @Body() body,
        @Response() res,
    ) {
        await this.employerService.updateBannerImage(body.user, file.filename);
        res.send({
            status: 200,
            message: 'Uploaded',
        });
    }

    @Post('delete-employer-logo')
    async deleteEmployerLogo(@Body() body) {
        await this.employerService.deleteEmployerLogo(body);
    }

    @Post('delete-employer-banner')
    async deleteEmployerBanner(@Body() body) {
        await this.employerService.deleteEmployerBanner(body);
    }

    @Get('banner/:imgpath')
    seeUploadedBannerFile(@Param('imgpath') image, @Res() res) {
        return res.sendFile(image, { root: './upload/employer/banner' });
    }

    @Get('/job-applications/:employerId')
    getJobApplications(@Param('employerId') employerId) {
        return this.employerService.getJobApplications(employerId);
    }

    @Get('/job-info/:jobId')
    getJobApplicantsInfo(@Param('jobId') jobId, @Request() req) {
        return this.employerService.getJobApplicantsInfo(jobId, req.query);
    }

    @Get('/list')
    getEmployers() {
        return this.employerService.getAllEmployers();
    }

    @Get('/list/:id')
    getEmployersAdminDetails(@Request() req) {
        return this.employerService.getEmployersInfo(req.params.id);
    }

    @Post('/profile/approve/:id')
    approveProfileChange(@Request() req) {
        return this.employerService.approveProfileChange(req.params.id);
    }

    @Post('/profile/reject/:id')
    rejectProfileChange(@Request() req) {
        return this.employerService.rejectProfileChange(
            req.params.id,
            req.body,
        );
    }
}
