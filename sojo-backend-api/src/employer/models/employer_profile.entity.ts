import {
    Column,
    Entity,
    JoinColumn,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { Employer } from './employer.entity';

@Entity('employer_profiles')
export class EmployerProfile {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
    })
    logo: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    aboutUs: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    scope: string;

    @Column({
        type: 'date',
        nullable: true,
    })
    info_founded_date: string;

    @Column({
        nullable: true,
    })
    info_headquaters: string;

    @Column({
        nullable: true,
    })
    info_company_size: number;

    @Column({
        nullable: true,
    })
    info_website_url: string;

    // @OneToOne(() => Employer, (employer) => employer.profile, {
    //     onDelete: 'CASCADE',
    // })
    // @JoinColumn()
    // employer: Employer;
}
