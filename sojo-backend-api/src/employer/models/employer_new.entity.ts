import {
    Column,
    Entity,
    OneToOne,
    PrimaryGeneratedColumn,
    JoinColumn,
} from 'typeorm';
import { User } from '../../user/models/user.entity';

@Entity('employers_new')
export class EmployerNew {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: true,
    })
    company_name: string;

    @Column({
        nullable: true,
    })
    address: string;

    @Column({
        nullable: true,
    })
    contact_no: string;

    @Column({
        nullable: true,
    })
    logo: string;

    @Column({
        nullable: true,
    })
    banner: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    aboutUs: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    founded_date: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    company_size: string;

    @Column({
        nullable: true,
    })
    website_url: string;

    @OneToOne(() => User, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    user: User;
}
