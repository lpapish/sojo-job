import {
    Column,
    Entity,
    OneToOne,
    PrimaryGeneratedColumn,
    JoinColumn,
} from 'typeorm';
import { User } from '../../user/models/user.entity';

@Entity('employers')
export class Employer {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: '',
    })
    company_name: string;

    @Column({
        default: '',
    })
    short_description: string;

    @Column({
        default: '',
    })
    address: string;

    @Column({
        default: '',
    })
    contact_no: string;

    @Column({
        default: '',
    })
    total_employee: string;

    @Column({
        nullable: true,
    })
    logo: string;

    @Column({
        nullable: true,
    })
    banner: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    aboutUs: string;

    @Column({
        type: 'date',
        nullable: true,
    })
    founded_date: string;

    @Column({
        nullable: true,
    })
    company_size: number;

    @Column({
        nullable: true,
    })
    website_url: string;

    @OneToOne(() => User, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    user: User;

    @Column({
        default: false,
    })
    is_verified: boolean;
}
