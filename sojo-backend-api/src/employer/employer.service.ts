import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { Employer } from './models/employer.entity';
import { MailService } from '../mail/mail.service';
import { User } from '../user/models/user.entity';
import { Job } from '../job/models/job.entity';
import { JobApplication } from 'src/job/models/job_applicantion.entity';
import { EmployerNew } from './models/employer_new.entity';
import { AwsService } from '../aws/aws.service';

@Injectable()
export class EmployerService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Employer)
        private readonly employerRepository: Repository<Employer>,
        @InjectRepository(EmployerNew)
        private readonly employerNewRepository: Repository<EmployerNew>,
        @InjectRepository(Job)
        private readonly jobRepository: Repository<Job>,
        @InjectRepository(JobApplication)
        private readonly jobApplicationRepository: Repository<JobApplication>,
        private jwtService: JwtService,
        private mailService: MailService,
        private awsService: AwsService,
    ) {}

    getEmployerInfo(id) {
        return this.employerRepository.findOne({
            where: {
                id,
            },
            relations: ['user'],
        });
    }

    async getEmployerDetailInfo(id) {
        const user = await this.userRepository.findOne({
            where: {
                id,
            },
            relations: ['employer', 'employerNew'],
        });
        return user;
    }

    async updateProfile(id, data) {
        const profile = await this.employerRepository.findOne({
            where: {
                user: id,
            },
        });
        profile.aboutUs = data.aboutUs;
        profile.founded_date = data.info_founded_date;
        profile.address = data.info_headquaters;
        profile.company_size = data.info_company_size;
        profile.website_url = data.info_website_url;

        return this.employerRepository.save(profile);
    }

    async updateProfileLogo(id, file) {
        const profile = await this.employerRepository.findOne({
            where: {
                employer: id,
            },
        });
        profile.logo = file.filename;

        return this.employerRepository.save(profile);
    }

    async updateEmployer(id, data, logo, banner) {
        let logoFile = null;
        let bannerFile = null;

        if (logo) {
            logoFile = await this.awsService.uploadImage(logo);
        }

        if (banner) {
            bannerFile = await this.awsService.uploadImage(banner);
        }

        const user = await this.userRepository.findOne({
            where: {
                id,
            },
        });

        const oldEmployerInfo = await this.employerRepository.findOne({
            where: {
                user,
            },
        });

        const employer = await this.employerNewRepository.findOne({
            where: {
                user,
            },
        });

        if (employer) {
            employer.company_name = data.company_name;
            employer.contact_no = data.contact_no;
            employer.aboutUs = data.aboutUs;
            employer.founded_date = data.founded_date;
            employer.address = data.address;
            employer.company_size = data.company_size;
            employer.logo =
                logoFile !== null ? logoFile?.Location : employer.logo;
            employer.banner =
                bannerFile !== null ? bannerFile?.Location : employer.banner;
            employer.website_url = data.website_url;

            return this.employerNewRepository.save(employer);
        }

        const newEmployer = await this.employerNewRepository.create({
            company_name: data.company_name,
            contact_no: data.contact_no,
            aboutUs: data.aboutUs,
            founded_date: data.founded_date,
            address: data.address,
            logo: logoFile !== null ? logoFile?.Location : oldEmployerInfo.logo,
            banner:
                bannerFile !== null
                    ? bannerFile?.Location
                    : oldEmployerInfo.banner,
            company_size: data.company_size,
            website_url: data.website_url,
            user,
        });

        return this.employerNewRepository.save(newEmployer);
    }

    async updateEmployerAdmin(id, data) {
        const user = await this.userRepository.findOne({
            where: {
                id,
            },
        });

        const employer = await this.employerRepository.findOne({
            where: {
                user,
            },
        });

        employer.company_name = data.company_name;
        employer.contact_no = data.contact_no;
        employer.aboutUs = data.aboutUs;
        employer.founded_date = data.founded_date;
        employer.address = data.address;
        employer.company_size = data.company_size;
        employer.logo = data.logo || null;
        employer.banner = data.banner || null;
        employer.website_url = data.website_url;
        await this.employerRepository.save(employer);
        this.mailService.sendEmployerProfileUpdateByAdminEmail(user.email);
    }

    register() {
        //
    }

    async login(user) {
        const payload = { sub: user.id, email: user.email };

        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async getUser(id) {
        const user = await this.employerRepository.findOne({
            where: {
                id,
            },
        });
        return { user };
    }

    async getJobs(userId) {
        const res = await this.userRepository.findOne({
            where: {
                id: userId,
            },
            relations: ['jobs'],
        });
        return res.jobs;
    }

    async getCompanyInfoWithJobs(id) {
        return this.employerRepository.findOne({
            where: {
                id,
            },
            relations: ['user', 'user.jobs'],
        });
    }

    async updateProfileImage(id, file) {
        const employer = await this.employerNewRepository.findOne({
            where: {
                user: id,
            },
        });
        if (employer) {
            employer.logo = file;
            employer.company_name = employer.company_name;
            employer.contact_no = employer.contact_no;
            employer.aboutUs = employer.aboutUs;
            employer.founded_date = employer.founded_date;
            employer.address = employer.address;
            employer.company_size = employer.company_size;
            employer.banner = employer.banner;
            employer.website_url = employer.website_url;
            return this.employerNewRepository.save(employer);
        }
    }

    async updateBannerImage(id, file) {
        const employer = await this.employerNewRepository.findOne({
            where: {
                user: id,
            },
        });
        employer.banner = file;
        employer.company_name = employer.company_name;
        employer.contact_no = employer.contact_no;
        employer.aboutUs = employer.aboutUs;
        employer.founded_date = employer.founded_date;
        employer.address = employer.address;
        employer.company_size = employer.company_size;
        employer.logo = employer.logo;
        employer.website_url = employer.website_url;
        return this.employerNewRepository.save(employer);
    }

    async getJobApplications(id) {
        return this.userRepository.findOne(id, {
            relations: ['jobs', 'jobs.applications'],
        });
    }

    // eslint-disable-next-line
    async getJobApplicantsInfo(id, query) {
        // if (query.status) {
        const whereQ: {
            format?: string;
            viewed: number;
        } = {
            format: '',
            viewed: 0,
        };

        if (query.status) {
            whereQ.format = query.status;
        } else {
            delete whereQ.format;
        }

        if (query.viewed) {
            whereQ.viewed = query.viewed === 'true' ? 1 : 0;
        }

        const job = await this.jobRepository.findOne(id);

        return this.jobApplicationRepository.find({
            where: {
                job,
                ...whereQ,
            },
            relations: ['user', 'questions', 'questions.job_question'],
        });

        // const builder = await this.jobRepository.createQueryBuilder('jobs');
        // await builder.where('jobs.id = :s', { s: id });
        // await builder.leftJoinAndSelect('jobs.applications', 'applications');
        // await builder.leftJoinAndSelect('applications', 'applications.user');

        // if (data.keyword) {
        //     builder.where('job.title LIKE :s', {
        //         s: `%${data.keyword}%`,
        //     });
        //     builder.orWhere('employers.company_name LIKE :s', {
        //         s: `%${data.keyword}%`,
        //     });
        // }

        // if (data.category) {
        //     builder.where('job.category = :s', { s: data.category });
        // }

        // if (data.education) {
        //     builder.where('job.educationId = :s', { s: data.education });
        // }

        // if (data.experience) {
        //     builder.where('job.experienceId = :s', { s: data.experience });
        // }

        // return await builder.getMany();
    }

    async deleteEmployerLogo(data) {
        const employer = await this.employerRepository.findOne({
            where: {
                user: data.user,
            },
        });

        employer.logo = '';
        return this.employerRepository.save(employer);
    }

    async deleteEmployerBanner(data) {
        const employer = await this.employerRepository.findOne({
            where: {
                user: data.user,
            },
        });

        employer.banner = '';
        return this.employerRepository.save(employer);
    }

    // admin service

    async getAllEmployers() {
        return this.userRepository.find({
            where: {
                user_type: 'employer',
            },
            relations: ['employer', 'employerNew'],
        });
    }

    async getEmployersInfo(id) {
        const user = await this.userRepository.findOne(id);

        const oldInfo = await this.userRepository.findOne(id, {
            relations: ['employer'],
        });

        const newInfo = await this.employerNewRepository.findOne({
            where: {
                user,
            },
        });

        return { oldInfo, newInfo };
    }

    async approveProfileChange(id) {
        const user = await this.userRepository.findOne(id);

        const newInfo = await this.employerNewRepository.findOne({
            where: {
                user,
            },
        });

        //
        const employer = await this.employerRepository.findOne({
            where: {
                user,
            },
        });

        employer.banner = newInfo.banner;
        employer.company_name = newInfo.company_name;
        employer.contact_no = newInfo.contact_no;
        employer.aboutUs = newInfo.aboutUs;
        employer.founded_date = newInfo.founded_date;
        employer.address = newInfo.address;
        employer.company_size = Number(newInfo.company_size);
        employer.logo = newInfo.logo;
        employer.website_url = newInfo.website_url;
        await this.employerRepository.save(employer);
        await this.employerNewRepository.delete(newInfo);
        this.mailService.sendEmployerProfileUpdateApprovedEmail(user.email);
    }

    async rejectProfileChange(employerId, data) {
        const user = await this.userRepository.findOne(employerId);

        const employerNewInfo = await this.employerNewRepository.findOne({
            where: {
                user,
            },
        });

        await this.mailService.sendEmployerProfileUpdateRejectEmail(
            user.email,
            data.reason,
        );
        await this.employerNewRepository.delete(employerNewInfo);
    }
}
