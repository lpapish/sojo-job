import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { EmployerController } from './employer.controller';
import { EmployerService } from './employer.service';
import { Employer } from './models/employer.entity';
import { EmployerNew } from './models/employer_new.entity';
import { MailService } from '../mail/mail.service';
import { User } from '../user/models/user.entity';
import { Job } from '../job/models/job.entity';
import { JobApplication } from 'src/job/models/job_applicantion.entity';
import { AwsService } from '../aws/aws.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Employer,
            EmployerNew,
            User,
            Job,
            JobApplication,
        ]),
        JwtModule.register({
            secret: 'some-random-secret',
            signOptions: { expiresIn: '60m' },
        }),
    ],
    controllers: [EmployerController],
    providers: [EmployerService, MailService, AwsService],
})
export class EmployerModule {}
