import {
    Controller,
    Get,
    Post,
    Request,
    Response,
    UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-strategy/jwt.guard';
import { AdminAuthService } from './admin_auth.service';

@Controller('api/admin/auth')
export class AdminAuthController {
    constructor(private adminAuthService: AdminAuthService) {}

    @Post('login')
    async login(@Request() req, @Response() res) {
        const token = await this.adminAuthService.login(req.body);
        return res.status(200).json({ access_token: token });
    }

    @UseGuards(JwtAuthGuard)
    @Get('user')
    async user(@Request() req) {
        return this.adminAuthService.getUser(req.user.sub);
    }

    @UseGuards(JwtAuthGuard)
    @Get('logout')
    async logout() {
        return {};
    }
}
