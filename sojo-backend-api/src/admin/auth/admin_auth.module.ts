import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminAuthController } from './admin_auth.controller';
import { AdminAuthService } from './admin_auth.service';
import { Admin } from './models/admin.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([Admin]),
        JwtModule.register({
            secret: 'some-random-secret',
            signOptions: { expiresIn: '60m' },
        }),
    ],
    providers: [AdminAuthService],
    controllers: [AdminAuthController],
})
export class AdminAuthModule {}
