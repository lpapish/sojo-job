import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('admin_config')
export class AdminConfig {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;
}
