import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Admin } from './models/admin.entity';

@Injectable()
export class AdminAuthService {
    constructor(
        @InjectRepository(Admin)
        private readonly adminRepository: Repository<Admin>,

        private jwtService: JwtService,
    ) {}

    async login({ email, password }) {
        const user = await this.adminRepository.findOne({
            where: { email, password },
        });

        if (user) {
            const payload = {
                sub: user.id,
                user,
            };

            return this.jwtService.sign(payload);
        }

        throw new UnauthorizedException();
    }

    async getUser(id: number) {
        const admin = await this.adminRepository.findOne(id);
        return { admin };
    }
}
