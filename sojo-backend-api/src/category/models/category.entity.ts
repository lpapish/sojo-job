import { Job } from 'src/job/models/job.entity';
import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
} from 'typeorm';
import { JobSeeker } from '../../job_seeker/models/job_seeker.entity';

@Entity('categories')
export class Category {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(() => Category, (category) => category.children, {
        nullable: true,
    })
    parent: Category;

    @OneToMany(() => Category, (category) => category.parent)
    children: Category[];

    @OneToMany(() => JobSeeker, (job_seeker) => job_seeker.category)
    jobSeeker: JobSeeker[];

    @OneToMany(() => Job, (job) => job.category)
    jobs: JobSeeker[];
}
