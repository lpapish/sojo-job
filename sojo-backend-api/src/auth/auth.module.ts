import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from '../user/models/user.entity';
import { LocalStrategy } from './local-strategy/local.strategy';
import { JwtStrategy } from './jwt-strategy/jwt.strategy';
import { JobSeeker } from '../job_seeker/models/job_seeker.entity';
import { Employer } from '../employer/models/employer.entity';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, JobSeeker, Employer]),
        JwtModule.register({
            secret: 'some-random-secret',
            signOptions: { expiresIn: '60m' },
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    controllers: [AuthController],
})
export class AuthModule {}
