import {
    Injectable,
    UnauthorizedException,
    BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { User } from '../user/models/user.entity';
import { JobSeeker } from '../job_seeker/models/job_seeker.entity';
import { Employer } from '../employer/models/employer.entity';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(JobSeeker)
        private readonly jobSeekerRepository: Repository<JobSeeker>,
        @InjectRepository(Employer)
        private readonly employerRepository: Repository<Employer>,
        private jwtService: JwtService,
    ) {}

    async validateUser({ email, password, role }) {
        const user = await this.userRepository.findOne({
            where: {
                email,
                user_type: role,
            },
        });

        if (user && user.password === password) {
            return user;
        }

        return null;
    }

    async login({ email, password, role }) {
        const user = await this.validateUser({ email, password, role });

        if (!user) {
            throw new UnauthorizedException();
        }

        const payload = {
            sub: user.id,
            email: user.email,
            role: user.user_type,
        };

        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async getUser(id) {
        const user = await this.userRepository.findOne({
            where: {
                id,
            },
        });
        return { user };
    }

    async register(data) {
        const userExist = await this.userRepository.findOne({
            where: {
                email: data.email,
            },
        });

        if (userExist) {
            throw new BadRequestException(
                'Account with this email already exists.',
            );
        }

        const userData = {
            username: data.fullname,
            email: data.email,
            password: data.password,
            user_type: data.user_type,
        };

        const newUser = await this.userRepository.create(userData);
        await this.userRepository.save(newUser);

        if (data.user_type === 'job_seeker') {
            const jobSeekerData = {
                contact_no: data.contact_no,
                fullname: data.fullname,
                user: newUser,
            };
            const jobSeeker = await this.jobSeekerRepository.create(
                jobSeekerData,
            );
            await this.jobSeekerRepository.save(jobSeeker);
        }

        if (data.user_type === 'employer') {
            const employerData = {
                company_name: data.company_name,
                contact_no: data.contact_no,
                user: newUser,
            };
            const employerProfile = await this.employerRepository.create(
                employerData,
            );
            await this.employerRepository.save(employerProfile);
        }
    }
}
