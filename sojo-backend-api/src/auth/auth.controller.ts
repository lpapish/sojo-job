import {
    Controller,
    Request,
    Post,
    Get,
    UseGuards,
    Body,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-strategy/jwt.guard';

@Controller('api/auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    /**
     * Handle login for both employer/job_seeker based on role
     *
     * @returns object wth jwt auth token
     */
    @Post('login')
    async login(@Body() body) {
        return await this.authService.login(body);
    }

    /**
     * Get Logged user info
     */
    @UseGuards(JwtAuthGuard)
    @Get('user')
    async user(@Request() req) {
        return this.authService.getUser(req.user.sub);
    }

    @UseGuards(JwtAuthGuard)
    @Get('logout')
    async logout() {
        return {};
    }

    @Post('register')
    async register(@Body() body) {
        return this.authService.register(body);
    }
}
