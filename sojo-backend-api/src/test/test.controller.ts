import { Controller, Get } from '@nestjs/common';

@Controller('api/test')
export class TestController {
    @Get('/')
    index() {
        return 'api is working';
    }
}
