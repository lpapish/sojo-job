import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
} from 'typeorm';
import { User } from '../../user/models/user.entity';

@Entity('companies')
export class Company {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    company_name: string;

    @Column()
    short_description: string;

    @Column()
    address: string;

    @Column({
        nullable: true,
    })
    logo: string;

    @Column({
        type: 'text',
        nullable: true,
    })
    aboutUs: string;

    @Column({
        type: 'date',
        nullable: true,
    })
    founded_date: string;

    @Column({
        nullable: true,
    })
    company_size: number;

    @Column({
        nullable: true,
    })
    website_url: string;

    @OneToOne(() => User, {
        onDelete: 'CASCADE',
    })
    @JoinColumn()
    user: User;
}
