import Vue from 'vue'
import VueTagsInput from '@johmun/vue-tags-input'
import VoerroTagsInput from '@voerro/vue-tagsinput'

Vue.use(VueTagsInput)
Vue.component('TagsInput', VoerroTagsInput)
