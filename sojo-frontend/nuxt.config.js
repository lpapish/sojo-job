export default {
  head: {
    title: 'sojojob',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  css: ['@/assets/css/main.scss'],

  plugins: [
    { src: '~/plugins/vue-fragment.js', mode: 'client' },
    { src: '~/plugins/router.js', mode: 'client' },
    '~/plugins/vuelidate.js',
    { src: '~/plugins/vue-toast.js', mode: 'client' },
    { src: '~plugins/vue-tag-input.js', ssr: false },
    { src: '@/plugins/vue-html2pdf', mode: 'client' },
  ],

  components: true,

  buildModules: ['@nuxtjs/eslint-module'],

  modules: ['@nuxtjs/axios', '@nuxtjs/auth-next', '@tui-nuxt/editor'],

  auth: {
    strategies: {
      user: {
        scheme: 'local',
        token: {
          property: 'access_token',
          required: true,
          type: 'Bearer',
        },
        user: {
          property: 'user',
        },
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
          },
          register: {
            url: 'auth/register',
            method: 'post',
          },
          logout: { url: 'auth/logout', method: 'get' },
          user: { url: 'auth/user', method: 'get' },
        },
      },
      admin: {
        scheme: 'local',
        token: {
          property: 'access_token',
          required: true,
          type: 'Bearer',
        },
        user: {
          property: 'admin',
        },
        endpoints: {
          login: {
            url: 'admin/auth/login',
            method: 'post',
          },
          user: { url: 'admin/auth/user', method: 'get' },
          logout: { url: 'admin/auth/logout', method: 'get' },
          redirect: {
            logout: '/admin/login',
          },
        },
      },
    },
  },

  axios: {
    baseURL: process.env.BASE_API_URL,
  },

  build: {},

  middleware: [],

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:9000',
  },
}
