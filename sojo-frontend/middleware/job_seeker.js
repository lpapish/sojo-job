export default function ({ store, redirect }) {
  if (!store.state.auth.loggedIn) {
    redirect('/')
  }

  if (
    store.state.auth &&
    store.state.auth.user &&
    store.state.auth.user.user_type !== 'job_seeker'
  ) {
    redirect('/')
  }
}
